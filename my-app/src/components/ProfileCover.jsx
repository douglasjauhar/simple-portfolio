import React from 'react'
import {Row, Col} from "antd"
import {InstagramOutlined, TwitterOutlined, LinkedinOutlined, GithubOutlined} from "@ant-design/icons"
import "./styles.css"

function redirect(el){
    alert(el)
}
function ProfileCover() {
    return (
        <div className="coverLeft">
            <div className="img__cover">
            <img src="https://i.ibb.co/MZhYQ10/52447313-removebg-preview-1.jpg" alt="" />
            </div>
            <div className="text__cover">
                <Row gutter={8} align={"middle"}>
                    <Col span={6}>
                    <InstagramOutlined onClick={() => redirect("ig")}/>
                    </Col>
                    <Col span={6}>
                    <TwitterOutlined onClick={() => redirect("tw")}/>
                    </Col>
                    <Col span={6}>
                    <LinkedinOutlined onClick={() => redirect("in")}/>
                    </Col>
                    <Col span={6}>
                    <GithubOutlined onClick={() => redirect("git")}/>
                    </Col>
                </Row>
            </div>
           
        
            
        </div>
    )
}
export default ProfileCover
