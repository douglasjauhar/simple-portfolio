import React from 'react'
import { Tabs, Button, Col, Row, Timeline } from "antd"
import "./styles.css"
import dataAbout from "./data/listData.json"
import dataEducation from "./data/educationData.json"
const { TabPane } = Tabs;
function callback(key) {
    console.log(key)
}
const Welcome = () => {
    return (
        <div className="welcome__">
            <h4>Hello, I'am </h4>
            <h3>M. Douglas Jauhar Nehru</h3>
            <h4>FRONT-END DEVELOPER</h4>
            <hr style={{ width: "70%" }} />
            <p className="desc__">A professional front-end developer from Jakarta, Indonesia. I always give the best effort for each projects I did. I give a solution with my creative app.</p>
            <br />
            <p style={{ fontWeight: "bold" }}>
                +62-81259-001-506
            </p>
            <p>douglasjauhar@gmail.com</p>

            <div className="btn__cv">
                <Button type="primary" ghost>Download CV</Button>
            </div>

        </div>
    )
}
const About = () => {
    return (
        <div className="about__">
            <h3 className="title">ABOUT <span style={{ fontWeight: "bold" }}>ME</span></h3>
            <div className="boxName">
                {dataAbout.map((row) => {
                    return (
                        <Row gutter={16}>
                            <Col xxl={12} xl={12} sm={12} style={{ fontWeight: "bold", textAlign: "left", float: "left" }}>
                                <i className={row.icon} style={{ marginRight: 10 }}></i>
                                {row.key}
                            </Col>
                            <Col xxl={12} xl={12} sm={12} style={{ textAlign: "right", float: "right" }} >
                                <p style={{ float: "right" }}>   {row.value}</p>

                            </Col>
                        </Row>
                    )
                })}
            </div>
            <div className="boxEducation" style={{margin : "auto"}}>
            <h3 className="title" style={{paddingBottom : 20}}><span style={{ fontWeight: "bold" }}>MY </span>EDUCATION</h3>
            <Row>
                <Col span={20}>
                <Timeline mode={"left"}>
                    {dataEducation.map((row) => {
                        return (
                            <Timeline.Item label={row.time}>
                                {row.education}
                                <br />
                                {row.desc}

                            </Timeline.Item>
                        )
                    })}
                </Timeline>
                </Col>
            </Row>
     
            </div>
            <div className="boxHobbies">

            </div>
        </div>
    )
}
function Contain() {
    return (
        <div className="coverRight">
            <Tabs defaultActiveKey="1"
                tabBarGutter={'0'}
                tabBarStyle={{ paddingRight: 0 }}
                onChange={callback}
            >
                <TabPane tab="Welcome" key="1">
                    <Welcome />
                </TabPane>
                <TabPane tab="Profile" key="2">
                    <About />
                </TabPane>
                <TabPane tab="Education" key="3">
                    Content of Tab Pane 3
              </TabPane>
                <TabPane tab="Resume" key="4">
                    Content of Tab Pane 4
              </TabPane>
                <TabPane tab="Portfolio" key="5">
                    Content of Tab Pane 5
              </TabPane>
                <TabPane tab="Contact" key="6">
                    Content of Tab Pane 6
             </TabPane>
            </Tabs>
        </div>
    )
}

export default Contain
