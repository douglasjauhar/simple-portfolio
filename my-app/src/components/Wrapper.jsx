import React, { Component } from 'react'
import {Row, Col, Card} from "antd"
import ProfileCover from "./ProfileCover"
import "./styles.css"
import Contain from './Contain'
export default class Wrapper extends Component {
    render() {
        return (
            <div className="wrape">
            <div className="wrapper__contain">
            <Card>
              <Row>
                  <Col xxl={8} xl={8} md={8}> 
                  <ProfileCover/>
                   </Col>
                  <Col xxl={16} xl={16} md={16}>
                      <Contain/>
                  </Col>
              </Row>
              </Card>
              </div>
            </div>
        )
    }
}
